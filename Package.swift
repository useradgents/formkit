// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "FormKit",
    platforms: [
        .iOS(.v13),
    ],
    products: [
        .library(name: "FormKit", targets: ["FormKit"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(name: "FormKit", dependencies: []),
        .testTarget(name: "FormKitTests", dependencies: ["FormKit"]),
    ]
)
