import UIKit

internal extension FormController {
    func setupTableView() {
        tableView?.delegate = self
        tableView?.dataSource = self

        tableView?.register(FormKitDefaultCell.self, forCellReuseIdentifier: FormKitDefaultCell.reuseIdentifier)
    }

    func fkNumberOfSections(in _: UITableView) -> Int {
        return sections.count
    }

    func fkTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }

    func fkTableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return sections[section].footerText
    }

    func fkTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count + (sections[section].canAddRows ? 1 : 0)
    }

    func fkTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if sections[indexPath.section].canAddRows, !sections[indexPath.section].rows.indices.contains(indexPath.row) {
            let addCell = tableView.dequeueReusableCell(withIdentifier: "add") ?? UITableViewCell(style: .default, reuseIdentifier: "add")
            addCell.textLabel?.text = sections[indexPath.section].addRowTitle ?? "Add"
            addCell.accessoryView = UIImageView(image: accessoryAddButtonImage())
            addCell.textLabel?.textColor = .secondaryLabel
            return addCell
        }
        
        let row = sections[indexPath.section].rows[indexPath.row]
        row.indexPath = indexPath
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FormKitDefaultCell.reuseIdentifier, for: indexPath)
        (cell as? FormCell)?.prepare(with: row)
        
        if let editedInstance = editedInstance {
            (cell as? FormCell)?.syncFrom(instance: editedInstance, to: row)
        }
        
        return cell
    }
    
    func fkTableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return sections[indexPath.section].canDeleteRows && sections[indexPath.section].rows.indices.contains(indexPath.row)
    }
    
    func fkTableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        guard editedInstance != nil else { return }
        let row = sections[indexPath.section].deleteRowForArray(of: &editedInstance!, at: indexPath.row)
        
        sections[indexPath.section].rows.removeAll()
        sections[indexPath.section].prepareRows(from: editedInstance!)
        tableView.reloadSections([indexPath.section], with: .none)
        rowDidEdit(row, isStillEditing: false, syncInput: false)
    }
    
    func fkTableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return sections[indexPath.section].canReorderRows && sections[indexPath.section].rows.indices.contains(indexPath.row)
    }
    
    func fkTableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if proposedDestinationIndexPath.section < sourceIndexPath.section {
            return IndexPath(row: 0, section: sourceIndexPath.section)
        }
        else if proposedDestinationIndexPath.section > sourceIndexPath.section {
            return IndexPath(row: sections[sourceIndexPath.section].rows.count - 1, section: sourceIndexPath.section)
        }
        else {
            return IndexPath(row: max(0, min(proposedDestinationIndexPath.row, sections[sourceIndexPath.section].rows.count - 1)), section: sourceIndexPath.section)
        }
    }
    
    func fkTableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // We need to let UIKit perform its dark magic first.
        // It we do it sync, the table view will break.
        DispatchQueue.main.async { [weak self] in
            self?.fkTableView(tableView, asyncMoveRowAt: sourceIndexPath, to: destinationIndexPath)
        }
    }
    
    func fkTableView(_ tableView: UITableView, asyncMoveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let row = sections[sourceIndexPath.section].moveRowForArray(of: &editedInstance!, from: sourceIndexPath.row, to: destinationIndexPath.row, inSectionAtIndex: sourceIndexPath.section)
        
        sections[sourceIndexPath.section].rows.removeAll()
        sections[sourceIndexPath.section].prepareRows(from: editedInstance!)
        tableView.reloadSections([sourceIndexPath.section], with: .none)
        rowDidEdit(row, isStillEditing: false, syncInput: false)
    }
    
    func fkTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        
        if section.canAddRows, !section.rows.indices.contains(indexPath.row) {
            tableView.deselectRow(at: indexPath, animated: true)
            guard editedInstance != nil else { return }
            let row = section.addNewRowToArray(of: &editedInstance!)
            row.input?.controller = self
            setupToolbar(for: row.input?.view as? UITextField)
            section.rows.append(row)
            updateFocusableFields()
            
            tableView.insertRows(at: [indexPath], with: .automatic)
            rowDidEdit(row, isStillEditing: false)
//            row.input?.didTapParentCell()
//            return
        }
        
        rowAtIndexPath(indexPath)?.input?.didTapParentCell()
    }
}
