import UIKit

// Default implementation is quite trivial

public protocol FormCell {
    static var reuseIdentifier: String { get }
    func prepare<Model>(with row: FormRow<Model>)
    func syncFrom<Model>(instance: Model, to row: FormRow<Model>)
}

extension FormCell {
    public static var reuseIdentifier: String { return String(describing: self) }
}

public class FormKitDefaultCell: UITableViewCell, FormCell {

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupSubviews()
    }

    public required init?(coder aDecoder: NSCoder) {
        return nil
    }

    deinit {
        prepareForReuse()
    }

    public override func prepareForReuse() {
        // Remove previous input
        fieldWrapper.subviews.first?.removeFromSuperview()
    }

    public func prepare<Model>(with row: FormRow<Model>) {
        iconView.image = row.image
        iconView.isHidden = (row.image == nil)

        titleLabel.text = row.title

        if let input = row.input {
            while let sub = fieldWrapper.subviews.first {
                sub.removeFromSuperview()
            }
            
            embedInput(input)
            
            if input.options.contains(.wantsDisclosureIndicator) {
                accessoryType = .disclosureIndicator
            }
            else {
                accessoryType = .none
            }
        }
    }

    public func syncFrom<Model>(instance: Model, to row: FormRow<Model>) {
        if let input = row.input {
            input.syncFromModelToInput?(instance, input)
        }
    }

    // Subviews

    /*

     ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
     │verticalStack                                                                                                    │
     │┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────┐│
     ││mainStack                                                                                                      ││
     ││┌────────┐┌─────────────────────────────────────────────────────┐┌──────────────────────────────────┐┌────────┐││
     │││iconView││titleLabel                                           ││fieldWrapper                      ││delete  │││
     │││        ││                                                     ││                                  ││Button  │││
     ││└────────┘└─────────────────────────────────────────────────────┘└──────────────────────────────────┘└────────┘││
     │└───────────────────────────────────────────────────────────────────────────────────────────────────────────────┘│
     │┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────┐│
     ││errorStack                                                                                                     ││
     ││┌────────┐┌───────────────────────────────────────────────────────────────────────────────────────────────────┐││
     │││error   ││errorLabel                                                                                         │││
     │││Spacer  ││                                                                                                   │││
     ││└────────┘└───────────────────────────────────────────────────────────────────────────────────────────────────┘││
     │└───────────────────────────────────────────────────────────────────────────────────────────────────────────────┘│
     └─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘

     */

    public var iconView: UIImageView = {
        let iv = UIImageView(image: nil)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .center
        iv.setContentHuggingPriority(.required, for: .horizontal)
        iv.setContentCompressionResistancePriority(.required, for: .horizontal)
        return iv
    }()

    public var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .secondaryLabel
        label.adjustsFontForContentSizeCategory = true
        label.widthAnchor.constraint(greaterThanOrEqualToConstant: 64).isActive = true
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 850), for: .horizontal)
        return label
    }()

    public var fieldWrapper: UIView = {
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.setContentHuggingPriority(UILayoutPriority(rawValue: 800), for: .horizontal)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        return v
    }()

    public var deleteButton: UIButton = {
        let b = UIButton(type: .custom)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("❌", for: .normal)
        b.setContentHuggingPriority(.required, for: .horizontal)
        b.setContentCompressionResistancePriority(.required, for: .horizontal)
        b.isHidden = true
        return b
    }()

    public var mainStack: UIStackView = {
        let s = UIStackView(frame: .zero)
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .horizontal
        s.alignment = .center
        s.spacing = 8
        
        let heightConstraint = s.heightAnchor.constraint(greaterThanOrEqualToConstant: 38)
        heightConstraint.priority = UILayoutPriority(999)
        heightConstraint.isActive = true
        
        return s
    }()

    public var errorSpacer: UIView = {
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    public var errorLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .body)
        label.textColor = .systemRed
        label.adjustsFontForContentSizeCategory = true
        return label
    }()

    public var errorStack: UIStackView = {
        let s = UIStackView(frame: .zero)
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .horizontal
        s.spacing = 8
        return s
    }()

    public var verticalStack: UIStackView = {
        let s = UIStackView(frame: .zero)
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .vertical
        s.spacing = 8
        return s
    }()

    func setupSubviews() {
        
        // Hierarchy
        mainStack.addArrangedSubview(iconView)
        mainStack.addArrangedSubview(titleLabel)
        mainStack.addArrangedSubview(fieldWrapper)
        mainStack.addArrangedSubview(deleteButton)

        errorStack.addArrangedSubview(errorSpacer)
        errorStack.addArrangedSubview(errorLabel)

        verticalStack.addArrangedSubview(mainStack)
        verticalStack.addArrangedSubview(errorStack)

        contentView.addSubview(verticalStack)

        // Constraints between views
        NSLayoutConstraint.activate([
            verticalStack.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            verticalStack.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            verticalStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            verticalStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 5),
        ])

        errorSpacer.widthAnchor.constraint(equalTo: iconView.widthAnchor).isActive = true
    }

    // Inputs

    func embedInput<Model>(_ input: FormInput<Model>) {
        fieldWrapper.addSubview(input.view)
        input.view.translatesAutoresizingMaskIntoConstraints = false
        input.view.sizeToFit()
        NSLayoutConstraint.activate([
            input.view.leadingAnchor.constraint(equalTo: fieldWrapper.leadingAnchor),
            input.view.trailingAnchor.constraint(equalTo: fieldWrapper.trailingAnchor),
            input.view.topAnchor.constraint(equalTo: fieldWrapper.topAnchor),
            input.view.bottomAnchor.constraint(equalTo: fieldWrapper.bottomAnchor)
        ])


        // Customization for common controls
        if let tf = input.view as? UITextField {
            tf.textColor = .label
            if let techInput = input as? FormTextField<Model>, techInput.isTechnical {
                tf.font = Self.technicalFont
            }
            else {
                tf.font = UIFont.preferredFont(forTextStyle: .body)
            }
            tf.textAlignment = .right
            if #available(iOS 10.0, *) {
                tf.adjustsFontForContentSizeCategory = true
            }
        }
    }
    
    private static var technicalFont: UIFont = {
        let descriptor = UIFont(name: ".AppleSystemUIFont", size: 17)!.fontDescriptor
        .addingAttributes([
            .featureSettings: [
                [
                    UIFontDescriptor.FeatureKey.featureIdentifier: kStylisticAlternativesType,
                    UIFontDescriptor.FeatureKey.typeIdentifier: kStylisticAltOneOnSelector
                ],
                [
                    UIFontDescriptor.FeatureKey.featureIdentifier: kStylisticAlternativesType,
                    UIFontDescriptor.FeatureKey.typeIdentifier: kStylisticAltTwoOnSelector
                ]
            ]
        ])
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont(descriptor: descriptor, size: 0))
    }()
}
