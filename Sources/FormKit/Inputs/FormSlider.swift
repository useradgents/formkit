import UIKit

public struct FormSliderOptions<T> where T: Numeric & Comparable {
    var range: ClosedRange<T>
    var step: T? // if nil, slider is discrete
    
    var valueLabelPosition: ValueLabelPosition = .afterSlider
    var valueLabelWidth: ValueLabelWidth = .automatic
    
    public enum ValueLabelPosition {
        case none
        //case aboveThumb // maybe one day
        //case belowThunb // maybe one day
        case beforeSlider // left on LTR locales
        case afterSlider // right on LTR locales
    }
    
    public enum ValueLabelWidth {
        case automatic
        case fixed(CGFloat)
    }
    
}

public class FormSlider<Model>: FormInput<Model> {
//    var slider: UISlider
//    var stack: UIStackView
//
//    public init<T>(keyPath: WritableKeyPath<Model, T>) where T: BinaryInteger {
//
//    }
//
}
