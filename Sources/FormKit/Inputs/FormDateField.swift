import UIKit

// Default value is used when focusing the input when the value is nil.
// A date picker must have a value, it cannot represent nil.
// If you provide nil as the default, "now" will be used instead (or zero for duration mode)
// There's no default to provide for non-optional variants, as obviously it won't be nil.

public class FormDateField<Model>: FormInput<Model>, UITextFieldDelegate {
    
    public struct Configuration {
        public var mode: UIDatePicker.Mode = .date
        public var dateStyle: DateFormatter.Style? = nil
        public var timeStyle: DateFormatter.Style? = nil
        public var dateFormat: String? = nil
        
        public var minDate: DateConvertible? = nil
        public var maxDate: DateConvertible? = nil
        public var defaultDateWhenUnset: Date? = nil
    }
    
    public init(keyPath: WritableKeyPath<Model, Date>, config: ((inout Configuration) -> Void)? = nil) {
        var configuration = Configuration()
        config?(&configuration)

        self.defaultWhenUnset = configuration.defaultDateWhenUnset
        self.picker = FormDateField.createPicker(mode: configuration.mode, min: configuration.minDate, max: configuration.maxDate)
        self.formatter = DateFormatter()
        self.formatter.dateStyle = configuration.dateStyle ?? .none
        self.formatter.timeStyle = configuration.timeStyle ?? .none
        if let df = configuration.dateFormat { self.formatter.dateFormat = df }
        
        super.init(FormTextFieldWithoutCaret(frame: .zero), [.interactive])
        setup(optional: false)
        
        syncFromModelToInput = { [weak self] model, input in
            guard let tf = input.view as? UITextField else { return }
            let date = model[keyPath: keyPath]
            self?.picker.date = date
            tf.text = self?.formatter.string(from: date)
        }
        
        syncFromInputToModel = { [weak self] input, model in
            guard let sself = self else { return }
            model[keyPath: keyPath] = sself.picker.date
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, Date?>, config: ((inout Configuration) -> Void)? = nil) {
        var configuration = Configuration()
        config?(&configuration)
        
        self.defaultWhenUnset = configuration.defaultDateWhenUnset
        self.picker = FormDateField.createPicker(mode: configuration.mode, min: configuration.minDate, max: configuration.maxDate)
        self.formatter = DateFormatter()
        self.formatter.dateStyle = configuration.dateStyle ?? .none
        self.formatter.timeStyle = configuration.timeStyle ?? .none
        if let df = configuration.dateFormat { self.formatter.dateFormat = df }
        
        super.init(FormTextFieldWithoutCaret(frame: .zero), [.interactive])
        setup(optional: true)
        
        syncFromModelToInput = { model, input in
            guard let sself = input as? FormDateField<Model> else { return }
            guard let tf = input.view as? UITextField else { return }
            
            sself.currentDate = model[keyPath: keyPath]
            if let date = model[keyPath: keyPath] {
                sself.picker.date = date
                tf.text = sself.formatter.string(from: date)
            }
            else {
                sself.picker.date = sself.defaultWhenUnset ?? Date()
                tf.text = nil
            }
        }
        
        syncFromInputToModel = { input, model in
            model[keyPath: keyPath] = (input as? FormDateField<Model>)?.currentDate
        }
    }
    
//    public init(keyPath: WritableKeyPath<Model, DateComponents>, mode: UIDatePickerMode, min: DateConvertible? = nil, max: DateConvertible? = nil) {
//        self.picker = FKDateField.createPicker(mode: mode, min: min, max: max)
//        self.formatter = FKDateField.createFormatter()
//        super.init(control: FKTextFieldWithoutCaret(frame: .zero))
//        setup(optional: false)
//    }
//
//    public init(keyPath: WritableKeyPath<Model, DateComponents?>, mode: UIDatePickerMode, min: DateConvertible? = nil, max: DateConvertible? = nil, defaultWhenUnset: DateComponents? = nil) {
//        self.picker = FKDateField.createPicker(mode: mode, min: min, max: max)
//        self.formatter = FKDateField.createFormatter()
//        self.defaultWhenUnset = defaultWhenUnset?.asDate
//        super.init(control: FKTextFieldWithoutCaret(frame: .zero))
//        setup(optional: false)
//    }
    
    internal func setup(optional: Bool) {
        guard let tf = view as? UITextField else { return }
        tf.clearButtonMode = optional ? .whileEditing : .never
        tf.delegate = self
        tf.inputView = picker
        picker.addTarget(self, action: #selector(datePickerDidChange(_:)), for: .valueChanged)
        isOptional = optional
    }
    
    private var currentDate: Date? {
        didSet {
            guard let tf = view as? UITextField else { return }
            if let d = currentDate {
                tf.text = formatter.string(from: d)
            }
            else {
                tf.text = nil
            }
        }
    }
    
    private var isOptional: Bool = false
    private var defaultWhenUnset: Date?
    private var picker: UIDatePicker
    private var formatter: DateFormatter
    
    public var formatterDateStyle: DateFormatter.Style {
        get { return formatter.dateStyle }
        set { formatter.dateStyle = newValue }
    }
    
    public var formatterTimeStyle: DateFormatter.Style {
        get { return formatter.timeStyle }
        set { formatter.timeStyle = newValue }
    }
    
    public var formatterDateFormat: String {
        get { return formatter.dateFormat }
        set { formatter.dateFormat = newValue }
    }
    
    
    static func createPicker(mode: UIDatePicker.Mode, min: DateConvertible?, max: DateConvertible?) -> UIDatePicker {
        let dp = UIDatePicker()
        if #available(iOS 13.4, *) {
            dp.preferredDatePickerStyle = .wheels
        }
        dp.datePickerMode = mode
        dp.minimumDate = min?.asDate
        dp.maximumDate = max?.asDate
        return dp
    }
    
    static func createFormatter() -> DateFormatter {
        let df = DateFormatter()
        df.dateStyle = .short
        df.timeStyle = .short
        return df
    }
    
    @objc func datePickerDidChange(_ sender: UIDatePicker) {
        currentDate = sender.date
        
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: true)
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        picker.date = currentDate ?? defaultWhenUnset ?? Date()
        
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidBeginEditing(row)
        
        if textField.text == nil || textField.text!.count == 0 {
            currentDate = picker.date
            controller?.rowDidEdit(row, isStillEditing: true)
        }
        
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard isOptional else { return false }
        
        currentDate = nil
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidEdit(row, isStillEditing: false)
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // No editing with the keyboard or with copy/paste, please
        return false
    }
}




public protocol DateConvertible {
    var asDate: Date? { get }
}

extension Date: DateConvertible {
    public var asDate: Date? {
        return self
    }
}

extension DateComponents: DateConvertible {
    public var asDate: Date? {
        return Calendar.current.date(from: self)
    }
}

extension TimeInterval: DateConvertible {
    public var asDate: Date? {
        return Date(timeIntervalSince1970: self)
    }
}

