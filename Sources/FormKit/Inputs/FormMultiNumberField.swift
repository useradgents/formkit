import UIKit

public class FormMultiNumberField<Model, MultiNumericType: FKMultiNumber>: FormInput<Model>, FKMultiTextFieldDelegate {
    private var emptyMeansNil: Bool = true
    
    public init(keyPath: WritableKeyPath<Model, MultiNumericType>) {
        let f = FKMultiTextField(numberOfFields: MultiNumericType.fkNumberOfFields, placeholders: MultiNumericType.fkFieldPlaceholers, separators: MultiNumericType.fkFieldSplitters)
        super.init(f, [.interactive])
        
        f.delegate = self
        f.clearButtonMode = .never
        f.keyboardType = MultiNumericType.isFloatingPoint ? .decimalPad : .numberPad
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? FKMultiTextField else { return }
            tf.texts = MultiNumericType.fkFieldMappings.map { "\(model[keyPath: keyPath][keyPath: $0].formKitNiceString)" }
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? FKMultiTextField else { return }
            guard let newValue = MultiNumericType(strings: tf.texts.compactMap({ $0 })) else { return}
            model[keyPath: keyPath] = newValue
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, MultiNumericType?>, emptyMeansNil: Bool = true) {
        self.emptyMeansNil = emptyMeansNil
        
        let f = FKMultiTextField(numberOfFields: MultiNumericType.fkNumberOfFields, placeholders: MultiNumericType.fkFieldPlaceholers, separators: MultiNumericType.fkFieldSplitters)
        super.init(f, [.interactive])
        
        f.delegate = self
        f.clearButtonMode = .always
        f.keyboardType = MultiNumericType.isFloatingPoint ? .decimalPad : .numberPad
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? FKMultiTextField else { return }
            if let value = model[keyPath: keyPath] {
                tf.texts = MultiNumericType.fkFieldMappings.map { "\(value[keyPath: $0].formKitNiceString)" }
            }
            else {
                tf.texts = Array(repeating: "", count: MultiNumericType.fkNumberOfFields)
            }
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormMultiNumberField<Model, MultiNumericType> else { return }
            guard let tf = i.view as? FKMultiTextField else { return }
            
            if let newValue = MultiNumericType(strings: tf.texts.compactMap({ $0 })) {
                model[keyPath: keyPath] = newValue
            }
            else {
                model[keyPath: keyPath] = i.emptyMeansNil ? nil : MultiNumericType.zero
            }
        }
    }
    
    public override func didTapParentCell() {
        view.becomeFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: FKMultiTextField) -> Bool {
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidBeginEditing(row)
        return true
    }
    
    func textFieldDidChangeEditing(_ textField: FKMultiTextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: true)
    }
    
    func textFieldDidEndEditing(_ textField: FKMultiTextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
}

public protocol FKMultiNumber {
    associatedtype FKComponent: FKNumber
    init?(strings: [String])
    static var fkNumberOfFields: Int { get }
    static var fkFieldPlaceholers: [String] { get }
    static var fkFieldSplitters: [String] { get }
    static var fkFieldMappings: [WritableKeyPath<Self, FKComponent>] { get }
    static var zero: Self { get }
    static var isFloatingPoint: Bool { get }
}

extension CGSize: FKMultiNumber {
    public typealias FKComponent = CGFloat
    public init?(strings: [String]) {
        guard strings.count == 2 else { return nil }
        guard let w = CGFloat.NativeType(strings[0]), let h = CGFloat.NativeType(strings[1]) else { return nil }
        self = CGSize(width: w, height: h)
    }
    public static let fkNumberOfFields = 2
    public static let fkFieldPlaceholers = ["width", "height"]
    public static let fkFieldSplitters = ["×"]
    public static let fkFieldMappings = [\CGSize.width, \CGSize.height]
//    public static let zero = CGSize.zero
    public static let isFloatingPoint = true
}

extension CGPoint: FKMultiNumber {
    public typealias FKComponent = CGFloat
    public init?(strings: [String]) {
        guard strings.count == 2 else { return nil }
        guard let x = CGFloat.NativeType(strings[0]), let y = CGFloat.NativeType(strings[1]) else { return nil }
        self = CGPoint(x: x, y: y)
    }
    public static let fkNumberOfFields = 2
    public static let fkFieldPlaceholers = ["x", "y"]
    public static let fkFieldSplitters = [", "]
    public static let fkFieldMappings = [\CGPoint.x, \CGPoint.y]
//    public static let zero = CGPoint.zero
    public static let isFloatingPoint: Bool = true
}

extension CGRect: FKMultiNumber {
    public typealias FKComponent = CGFloat
    public init?(strings: [String]) {
        guard strings.count == 4 else { return nil }
        guard let w = CGFloat.NativeType(strings[0]), let h = CGFloat.NativeType(strings[1]), let x = CGFloat.NativeType(strings[2]), let y = CGFloat.NativeType(strings[3]) else { return nil }
        self = CGRect(x: x, y: y, width: w, height: h)
    }
    public static let fkNumberOfFields: Int = 4
    public static let fkFieldPlaceholers = ["w", "h", "x", "y"]
    public static let fkFieldSplitters = ["×", "@", ", "]
    public static let fkFieldMappings = [\CGRect.size.width, \CGRect.size.height, \CGRect.origin.x, \CGRect.origin.y]
//    public static let zero = CGRect.zero
    public static let isFloatingPoint: Bool = true
}

// Not for Range. Bounds are not writable as-is.
//extension Range: FKMultiNumber {
//    public typealias FKComponent = Bound
//    public static var fkNumberOfFields: Int { return 2 }
//    public static var fkFieldPlaceholers: [String] { return ["from", "to"] }
//    public static var fkFieldSplitters: [String] { return ["..."] }
//    public static var fkFieldMappings: [WritableKeyPath<Range, Bound>] { return [\Range<Bound>.lowerBound, \Range<Bound>.upperBound] }
//}




class FKTextField: UITextField {
    override var intrinsicContentSize: CGSize {
        let s: String
        if let t = text, !t.isEmpty { s = t }
        else if let p = placeholder, !p.isEmpty { s = p }
        else { s = "" }
        var size = NSAttributedString(string: s, attributes: [.font: font as Any]).size()
        
        if clearButtonMode == .always || (isFirstResponder && clearButtonMode == .whileEditing) { size.width += clearButtonRect(forBounds: bounds).width + 8 }
        
        size.width += 1 // dunno why, else it will truncate the text
        
        if size.width < 16 { size.width = 16 }
        
        return size
    }
}

@objc protocol FKMultiTextFieldDelegate: AnyObject {
    @objc optional func textFieldShouldBeginEditing(_ textField: FKMultiTextField) -> Bool
    @objc optional func textFieldDidEndEditing(_ textField: FKMultiTextField)
    @objc optional func textFieldDidChangeEditing(_ textField: FKMultiTextField)
    // we won't be using the other ones, no need to replicate them
}

class FKMultiTextField: UIView, UITextFieldDelegate {
    private let textFields: [FKTextField]
    private let separators: [UILabel]
    private let stack: UIStackView
    
    weak var delegate: FKMultiTextFieldDelegate?
    
    init(numberOfFields: Int, placeholders: [String]? = nil, separators: [String]? = nil) {
        self.textFields = (0 ..< numberOfFields).map {
            let t = FKTextField(frame: .zero)
            t.placeholder = (placeholders?.indices.contains($0) ?? false) ? placeholders![$0] : nil
//            t.clearButtonMode = ($0 == numberOfFields - 1) ? .always : .never
//            t.backgroundColor = .yellow
            t.setContentHuggingPriority(.required, for: .horizontal)
            t.setContentCompressionResistancePriority(.required, for: .horizontal)
            return t
        }
        
        self.separators = separators?.map {
            let l = UILabel(frame: .zero)
            l.text = $0
            l.textColor = .tertiaryLabel
            l.setContentHuggingPriority(.required, for: .horizontal)
            l.setContentCompressionResistancePriority(.required, for: .horizontal)
            return l
        } ?? []
        
        
        self.stack = UIStackView(frame: .zero)
        stack.translatesAutoresizingMaskIntoConstraints = false
        for i in self.textFields.indices {
            stack.addArrangedSubview(self.textFields[i])
            if self.separators.indices.contains(i) {
                stack.addArrangedSubview(self.separators[i])
            }
        }
        
        super.init(frame: .zero)
        self.addSubview(stack)
        NSLayoutConstraint.activate([
            stack.leftAnchor.constraint(equalTo: self.leftAnchor),
            stack.rightAnchor.constraint(equalTo: self.rightAnchor),
            stack.topAnchor.constraint(equalTo: self.topAnchor),
            stack.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
        
        for i in self.textFields.indices {
            self.textFields[i].delegate = self
            self.textFields[i].addTarget(self, action: #selector(textFieldDidChangeEditing(_:)), for: .editingChanged)
        }
    }
    
    var keyboardType: UIKeyboardType {
        get { return textFields.first?.keyboardType ?? .default }
        set { textFields.forEach { $0.keyboardType = newValue } }
    }
    
    var clearButtonMode: UITextField.ViewMode {
        get { return textFields.last?.clearButtonMode ?? .never }
        set { textFields.last?.clearButtonMode = newValue; setNeedsLayout() }
    }
    
    var texts: [String?] {
        get { return textFields.map({$0.text}) }
        set { textFields.enumerated().forEach {
            guard newValue.indices.contains($0) else { return }
            $1.text = newValue[$0]
        }}
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("🤷🏻‍♂️")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // If we're already editing a friend, blindly return yes without asking the delegate
        if textFields.first(where: { $0.isEditing && $0 !== textField }) != nil { return true }
        return delegate?.textFieldShouldBeginEditing?(self) ?? true
    }
    
    @objc private func textFieldDidChangeEditing(_ textField: UITextField) {
        delegate?.textFieldDidChangeEditing?(self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // If we're editing a fried now, do nothing
        if textFields.first(where: { $0.isEditing && $0 !== textField }) != nil { return }
        delegate?.textFieldDidEndEditing?(self)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard textField == textFields.last else { return false }
        textFields.forEach({ $0.text = nil })
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.count > 0 else { return true }
        let text = textField.text ?? ""
        guard let textRange = Range(range, in: text) else { return true }
        
        let newText = (textField.text ?? "").replacingCharacters(in: textRange, with: string)
        if FKMultiTextField.allowedCharacters.isSuperset(of: CharacterSet(charactersIn: newText)) {
            return true
        }
        else if let tf = textField as? FKTextField, let index = textFields.firstIndex(of: tf), textFields.indices.contains(index + 1) {
            textFields[index + 1].becomeFirstResponder()
        }
        return false
    }
    
    private static var allowedCharacters: CharacterSet = CharacterSet(charactersIn: "0123456789 .,")
    
    override var canBecomeFirstResponder: Bool { return true }
    override func becomeFirstResponder() -> Bool {
        let _ = textFields.first(where: { $0.becomeFirstResponder() })
        return false
    }
    
    override func resignFirstResponder() -> Bool {
        textFields.forEach({ $0.resignFirstResponder() })
        return false
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(
            width: textFields.reduce(CGFloat(0), { $0 + $1.intrinsicContentSize.width }) + separators.reduce(CGFloat(0), { $0 + $1.intrinsicContentSize.width }),
            height: textFields.first?.intrinsicContentSize.height ?? 29
        )
    }
    
    
}
