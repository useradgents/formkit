import UIKit

public class FormSegment<Model>: FormInput<Model> {
    
    /* WritableKeyPath<Model, T>, possible T's:
     *   FKListable             -- exactly 1 out of N
     *   FKListable?            -- [0, 1] out of N
     *   OptionSet & FKListable -- [0..N] out of N              -- does not handle Optionals, only empty set
     *   Array<FKMultiListable> -- [T.minChoices..T.maxChoices] -- does not handle Optionals, only empty array
     */
    
    //MARK: - FKListable
    public init<T: Listable>(getter: @escaping () -> T, setter: @escaping (T) -> ()) {
        let seg = UISegmentedControl(items: T.allNames)
        super.init(seg, [.compact])
        seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
        
        syncFromModelToInput = { model, input in
            guard let seg = input.view as? UISegmentedControl else { return }
            seg.selectedSegmentIndex = T.allValues.firstIndex(of: getter()) ?? UISegmentedControl.noSegment
        }
        
        syncFromInputToModel = { input, model in
            guard let seg = input.view as? UISegmentedControl else { return }
            setter(T.allValues[seg.selectedSegmentIndex])
        }
    }
    
    public init<T: Listable>(keyPath: WritableKeyPath<Model, T>) {
        let seg = UISegmentedControl(items: T.allNames)
        super.init(seg, [.compact])
        seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
        
        syncFromModelToInput = { model, input in
            guard let seg = input.view as? UISegmentedControl else { return }
            let value: T = model[keyPath: keyPath]
            if let index = T.allValues.firstIndex(of: value) {
                seg.selectedSegmentIndex = index
            }
            else {
                // should never happen, but who knows
                seg.selectedSegmentIndex = UISegmentedControl.noSegment
            }
        }
        
        syncFromInputToModel = { input, model in
            guard let seg = input.view as? UISegmentedControl else { return }
            model[keyPath: keyPath] = T.allValues[seg.selectedSegmentIndex]
        }
    }
    
    //MARK: - FKListable?
    public init<T: Listable>(keyPath: WritableKeyPath<Model, T?>) {
        if case .noSelection(_) = T.nilMode {
            let seg = FormSegmentedControl(items: T.allNames)
            super.init(seg, [.compact])
            seg.minimumSelectedSegmentsCount = 0
            seg.maximumSelectedSegmentsCount = 1
            seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
            
            syncFromModelToInput = { model, input in
                guard let seg = input.view as? FormSegmentedControl else { return }
                if let value: T = model[keyPath: keyPath], let index = T.allValues.firstIndex(of: value) {
                    seg.selectedSegmentIndex = index
                }
                else {
                    seg.selectedSegmentIndex = UISegmentedControl.noSegment
                }
            }
            
            syncFromInputToModel = { input, model in
                guard let seg = input.view as? FormSegmentedControl else { return }
                if seg.selectedSegmentIndex == UISegmentedControl.noSegment {
                    model[keyPath: keyPath] = nil
                }
                else {
                    model[keyPath: keyPath] = T.allValues[seg.selectedSegmentIndex]
                }
            }
        }
        else {
            var items = T.allNames
            switch T.nilMode {
                case let .first(nilName):
                    items.insert(nilName, at: 0)
                
                case let .last(nilName):
                    items.append(nilName)
                
                default: break
            }
            let seg = FormSegmentedControl(items: items)
            super.init(seg, [.compact])
            seg.minimumSelectedSegmentsCount = 1
            seg.maximumSelectedSegmentsCount = 1
            seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
            
            switch T.nilMode {
            case .first(_):
                syncFromModelToInput = { model, input in
                    guard let seg = input.view as? FormSegmentedControl else { return }
                    if let value: T = model[keyPath: keyPath], let index = T.allValues.firstIndex(of: value) {
                        seg.selectedSegmentIndex = index + 1
                    }
                    else {
                        seg.selectedSegmentIndex = 0
                    }
                }
                
                syncFromInputToModel = { input, model in
                    guard let seg = input.view as? FormSegmentedControl else { return }
                    if seg.selectedSegmentIndex == 0 {
                        model[keyPath: keyPath] = nil
                    }
                    else {
                        model[keyPath: keyPath] = T.allValues[seg.selectedSegmentIndex - 1]
                    }
                }
                
            case .last(_):
                syncFromModelToInput = { model, input in
                    guard let seg = input.view as? FormSegmentedControl else { return }
                    if let value: T = model[keyPath: keyPath], let index = T.allValues.firstIndex(of: value) {
                        seg.selectedSegmentIndex = index
                    }
                    else {
                        seg.selectedSegmentIndex = seg.numberOfSegments - 1
                    }
                }
                
                syncFromInputToModel = { input, model in
                    guard let seg = input.view as? FormSegmentedControl else { return }
                    if seg.selectedSegmentIndex == seg.numberOfSegments - 1 {
                        model[keyPath: keyPath] = nil
                    }
                    else {
                        model[keyPath: keyPath] = T.allValues[seg.selectedSegmentIndex]
                    }
                }
                
            default: break
            }
        }
    }
    
    //MARK: - Array<FKMultiListable>
    public init<T: MultiSelectionListable>(keyPath: WritableKeyPath<Model, Array<T>>) {
        let seg = FormSegmentedControl(items: T.allNames)
        super.init(seg, [.compact])
        seg.minimumSelectedSegmentsCount = T.minChoices
        seg.maximumSelectedSegmentsCount = T.maxChoices
        seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
        
        syncFromModelToInput = { model, input in
            guard let seg = input.view as? FormSegmentedControl else { return }
            let values = model[keyPath: keyPath]
            let indexes = values.compactMap( T.allValues.firstIndex )
            seg.selectedSegmentIndexes = IndexSet(indexes)
        }
        
        syncFromInputToModel = { input, model in
            guard let seg = input.view as? FormSegmentedControl else { return }
            model[keyPath: keyPath] = seg.selectedSegmentIndexes.map({ T.allValues[$0] })
        }
    }
    
    //MARK: - OptionSet & FKListable
    public init<T: OptionSet & Listable>(keyPath: WritableKeyPath<Model, T>) {
        let seg = FormSegmentedControl(items: T.allNames)
        super.init(seg, [.compact])
        seg.minimumSelectedSegmentsCount = 0
        seg.maximumSelectedSegmentsCount = Int.max
        seg.addTarget(self, action: #selector(didChangeSegmentValue(_:)), for: .valueChanged)
        
        syncFromModelToInput = { model, input in
            guard let seg = input.view as? FormSegmentedControl else { return }
            let value: T = model[keyPath: keyPath]
            
            seg.selectedSegmentIndexes = IndexSet(T.allValues.enumerated().compactMap({
                return value.isSuperset(of: $0.element) ? $0.offset : nil
            }))
        }
        
        syncFromInputToModel = { input, model in
            guard let seg = input.view as? FormSegmentedControl else { return }
            var value: T = []
            seg.selectedSegmentIndexes.forEach({ value.formUnion(T.allValues[$0]) })
            model[keyPath: keyPath] = value
        }
    }
    
    //MARK: -
    
    @objc func didChangeSegmentValue(_ sender: FormSegmentedControl) {
        guard let c = controller else { return }
        guard let row = c.rowWithControl(sender) else { return }
        
        c.rowDidEdit(row, isStillEditing: false)
    }
}
