import UIKit

public class FormTextField<Model>: FormInput<Model>, UITextFieldDelegate {
    
    private var emptyMeansNil: Bool = true
    internal var isTechnical: Bool = false
    
    public init(getter: @escaping () -> String?, setter: @escaping (String?) -> (), configurator: ((UITextField)->())? = nil) {
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = getter()
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            setter(tf.text)
        }
    }
    
    public init(getter: @escaping () -> String, setter: @escaping (String) -> (), configurator: ((UITextField)->())? = nil) {
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { mode, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = getter()
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            setter(tf.text ?? "")
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, String>, configurator: ((UITextField)->())? = nil) {
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath]
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            model[keyPath: keyPath] = tf.text ?? ""
        }
    }
    
    public init<Custom>(keyPath: WritableKeyPath<Model, Custom>, configurator: ((UITextField)->())? = nil) where Custom: CustomStringConvertible & ExpressibleByStringLiteral, Custom.StringLiteralType == String {
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath].description
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            model[keyPath: keyPath] = Custom.init(stringLiteral: tf.text ?? "")
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, URL>, configurator: ((UITextField)->())? = nil) {
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath].absoluteString
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            if let text = tf.text, let url = URL(string: text) {
                model[keyPath: keyPath] = url
            }
            else {
                model[keyPath: keyPath] = URL(string: "about://blank")!
            }
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, String?>, emptyMeansNil: Bool = true, configurator: ((UITextField)->())? = nil) {
        self.emptyMeansNil = emptyMeansNil
        
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath]
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormTextField<Model> else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, !text.isEmpty {
                model[keyPath: keyPath] = text
            }
            else {
                model[keyPath: keyPath] = i.emptyMeansNil ? nil : ""
            }
        }
    }
    
    public init<Custom>(keyPath: WritableKeyPath<Model, Custom?>, emptyMeansNil: Bool = true, configurator: ((UITextField)->())? = nil) where Custom: CustomStringConvertible & ExpressibleByStringLiteral, Custom.StringLiteralType == String {
        self.emptyMeansNil = emptyMeansNil
        
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath]?.description
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormTextField<Model> else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, !text.isEmpty {
                model[keyPath: keyPath] = Custom.init(stringLiteral: text)
            }
            else {
                model[keyPath: keyPath] = i.emptyMeansNil ? nil : Custom.init(stringLiteral: "")
            }
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, URL?>, configurator: ((UITextField)->())? = nil) {
        self.emptyMeansNil = true
        
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath]?.absoluteString
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormTextField<Model> else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, let url = URL(string: text) {
                model[keyPath: keyPath] = url
            }
            else {
                model[keyPath: keyPath] = nil
            }
        }
    }
    
    internal func setup(numeric: Bool = false) {
        guard let tf = view as? UITextField else { return }
        tf.clearButtonMode = .whileEditing
        if numeric {
            tf.keyboardType = .numberPad
        }
        tf.delegate = self
        tf.addTarget(self, action: #selector(textFieldDidChangeEditing(_:)), for: .editingChanged)
    }
    
    public override func didTapParentCell() {
        view.becomeFirstResponder()
    }
    
    //MARK: - Text field delegate
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidBeginEditing(row)
        return true
    }

    @objc private func textFieldDidChangeEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: true)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
}


public enum FormTextFieldTemplate {
    case email
    case name
    case passwordAsReallyUnsecureClearText // hope this will discourage you from using this template.
    case integerNumber
    case decimalNumber
    case phone
    case URL
    case technical // monospace, for serial numbers, mac addresses, …
    case unspecified
}

public extension FormTextField {
    internal static func convenienceConfigurator(tf: UITextField, template: FormTextFieldTemplate, placeholder: String?) {
        tf.placeholder = placeholder
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        switch template {
        case .email:
            tf.keyboardType = .emailAddress
            
        case .name:
            tf.autocapitalizationType = .words
            
        case .passwordAsReallyUnsecureClearText:
            tf.isSecureTextEntry = true
            print("🔥🚨 🆆🅰🆁🅽🅸🅽🅶 💣🧨 Using `passwordAsReallyUnsecureClearText` is a very bad idea. You should use a FormPasswordField with a hashing function instead.")
        
        case .integerNumber:
            tf.keyboardType = .numberPad
        
        case .decimalNumber:
            tf.keyboardType = .decimalPad
            
        case .phone:
            tf.keyboardType = .phonePad
            
        case .URL:
            tf.keyboardType = .URL
            
        default:
            break
        }
    }
    
    convenience init(keyPath: WritableKeyPath<Model, String>, _ template: FormTextFieldTemplate = .unspecified, placeholder: String? = nil) {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
    
    convenience init<Custom>(keyPath: WritableKeyPath<Model, Custom>, _ template: FormTextFieldTemplate = .unspecified, placeholder: String? = nil) where Custom: CustomStringConvertible & ExpressibleByStringLiteral, Custom.StringLiteralType == String {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
    
    convenience init(keyPath: WritableKeyPath<Model, URL>, _ template: FormTextFieldTemplate = .URL, placeholder: String? = nil) {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
    
    convenience init(keyPath: WritableKeyPath<Model, String?>, _ template: FormTextFieldTemplate = .unspecified, placeholder: String? = nil) {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
    
    convenience init<Custom>(keyPath: WritableKeyPath<Model, Custom?>, _ template: FormTextFieldTemplate = .unspecified, placeholder: String? = nil) where Custom: CustomStringConvertible & ExpressibleByStringLiteral, Custom.StringLiteralType == String {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
    
    convenience init(keyPath: WritableKeyPath<Model, URL?>, _ template: FormTextFieldTemplate = .URL, placeholder: String? = nil) {
        self.init(keyPath: keyPath) { FormTextField.convenienceConfigurator(tf: $0, template: template, placeholder: placeholder) }
        isTechnical = (template == .technical)
    }
}
