import UIKit

public class FormNumberField<Model, NumericType: FKNumber>: FormInput<Model>, UITextFieldDelegate {
    private var emptyMeansNil: Bool = true
    private var isFloatingPoint: Bool

    public init(getter: @escaping () -> NumericType, setter: @escaping (NumericType) -> (), placeholder: String? = nil, suffix: String? = nil) {
        self.isFloatingPoint = NumericType.isFloatingPoint
        
        let f = UITextField(frame: .zero)
        f.placeholder = placeholder
        super.init(f, [.interactive])
        
        setup()
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = getter().formKitNiceString
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            setter(NumericType(fkString: tf.text ?? "0"))
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, NumericType>, placeholder: String? = nil, suffix: String? = nil) {
        self.isFloatingPoint = NumericType.isFloatingPoint
        
        let f = UITextField(frame: .zero)
        f.placeholder = placeholder
        super.init(f, [.interactive])
        
        setup()
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath].formKitNiceString
        }
        
        syncFromInputToModel = { input, model in
            guard let tf = input.view as? UITextField else { return }
            model[keyPath: keyPath] = NumericType(fkString: tf.text ?? "0")
        }
    }
    
    public init(getter: @escaping () -> NumericType?, setter: @escaping (NumericType?) -> (), emptyMeansNil: Bool = true, placeholder: String? = nil, suffix: String? = nil) {
        self.emptyMeansNil = emptyMeansNil
        self.isFloatingPoint = NumericType.isFloatingPoint
        
        let f = UITextField(frame: .zero)
        f.placeholder = placeholder
        super.init(f, [.interactive])
        
        setup()
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = getter().map({$0.formKitNiceString}) ?? ""
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormNumberField<Model, NumericType> else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, !text.isEmpty {
                setter(NumericType(fkString: text))
            }
            else {
                setter(i.emptyMeansNil ? nil : NumericType.zero)
            }
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, NumericType?>, emptyMeansNil: Bool = true, placeholder: String? = nil, suffix: String? = nil) {
        self.emptyMeansNil = emptyMeansNil
        self.isFloatingPoint = NumericType.isFloatingPoint
        
        let f = UITextField(frame: .zero)
        f.placeholder = placeholder
        super.init(f, [.interactive])
        
        setup()
        
        syncFromModelToInput = { model, input in
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath].map({$0.formKitNiceString}) ?? ""
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormNumberField<Model, NumericType> else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, !text.isEmpty {
                model[keyPath: keyPath] = NumericType(fkString: text)
            }
            else {
                model[keyPath: keyPath] = i.emptyMeansNil ? nil : NumericType.zero
            }
        }
    }
    
    internal func setup() {
        guard let tf = view as? UITextField else { return }
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = isFloatingPoint ? .decimalPad : .numberPad
        tf.delegate = self
        tf.addTarget(self, action: #selector(textFieldDidChangeEditing(_:)), for: .editingChanged)
    }
    
    public override func didTapParentCell() {
        view.becomeFirstResponder()
    }
    
    //MARK: - Text field delegate
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidBeginEditing(row)
        return true
    }
    
    @objc private func textFieldDidChangeEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: true)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
}



public protocol FKNumber {
    init(fkString: String)
    static var zero: Self { get }
    static var isFloatingPoint: Bool { get }
    var formKitNiceString: String { get }
}

public protocol FKIntegerNumber: FKNumber {}
public protocol FKFloatingPointNumber: FKNumber {}

extension Int: FKIntegerNumber {
    public init(fkString: String) { self = Int(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = Int(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension Int8: FKIntegerNumber {
    public init(fkString: String) { self = Int8(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = Int8(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension Int16: FKIntegerNumber {
    public init(fkString: String) { self = Int16(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = Int16(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension Int32: FKIntegerNumber {
    public init(fkString: String) { self = Int32(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = Int32(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension Int64: FKIntegerNumber {
    public init(fkString: String) { self = Int64(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = Int64(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension UInt: FKIntegerNumber {
    public init(fkString: String) { self = UInt(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = UInt(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension UInt8: FKIntegerNumber {
    public init(fkString: String) { self = UInt8(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = UInt8(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension UInt16: FKIntegerNumber {
    public init(fkString: String) { self = UInt16(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = UInt16(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension UInt32: FKIntegerNumber {
    public init(fkString: String) { self = UInt32(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = UInt32(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension UInt64: FKIntegerNumber {
    public init(fkString: String) { self = UInt64(fkString.replacingOccurrences(of: " ", with: "")) ?? 0 }
    public static let zero = UInt64(0)
    public static let isFloatingPoint = false
    public var formKitNiceString: String { return "\(self)" }
}

extension CGFloat: FKFloatingPointNumber {
    public init(fkString: String) { self = CGFloat(CGFloat.NativeType(fkString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")) ?? 0) }
    public static let zero = CGFloat(0)
    public static let isFloatingPoint = true
    
    public var formKitNiceString: String {
        if ceil(self) == floor(self) {
            return String(Int(ceil(self)))
        }
        else {
            return "\(self)"
        }
    }
}

extension Float32: FKFloatingPointNumber {
    public init(fkString: String) { self = Float32(fkString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")) ?? 0 }
    public static let zero = Float32(0)
    public static let isFloatingPoint = true
    
    public var formKitNiceString: String {
        if ceil(self) == floor(self) {
            return String(Int(ceil(self)))
        }
        else {
            return "\(self)"
        }
    }
}

extension Float64: FKFloatingPointNumber {
    public init(fkString: String) { self = Float64(fkString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")) ?? 0 }
    public static let zero = Float64(0)
    public static let isFloatingPoint = true
    
    public var formKitNiceString: String {
        if ceil(self) == floor(self) {
            return String(Int(ceil(self)))
        }
        else {
            return "\(self)"
        }
    }
}

#if os(macOS)
extension Float80: FKFloatingPointNumber {
    public init(fkString: String) { self = Float80(fkString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")) ?? 0 }
    public static let zero = Float80(0)
    public static let isFloatingPoint = true
    
    public var formKitNiceString: String {
        if ceil(self) == floor(self) {
            return String(Int(ceil(self)))
        }
        else {
            return "\(self)"
        }
    }
}
#endif
