import UIKit

public struct FormListConfiguration {
    public var nilMode: ListableNilMode = .noSelection(nil)
    
    public var minChoices: Int = 1
    public var maxChoices: Int = Int.max
    public var separator: String = ", " // for multiple items in text field
    
    public var minimumNumberOfItemsToDisplaySearchBar: Int = 10
    public var reloadListEveryTime: Bool = true
    public var showIndentationTree: Bool = true // TODO... sometime...
    public var flattenWhenSearching: Bool = true
    public var flattenInField: Bool = true
    public var flattener: String = " › " // for item trees
}

public struct FormListItem<Value> {
    public var name: String
    public var image: UIImage?
    public var value: Value
    public var indentationLevel: Int
    public var selectable: Bool// use this to make only leaves selectable, for example in a continent > country selector?
    
    public init(name: String, image: UIImage? = nil, value: Value, indentationLevel: Int = 0, selectable: Bool = true) {
        self.name = name
        self.image = image
        self.value = value
        self.indentationLevel = indentationLevel
        self.selectable = selectable
    }
}


public class FormListField<Model, Value>: FormInput<Model>, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate where Value: Equatable {
    
    public typealias Item = FormListItem<Value>
    public typealias Items = [Item]
    public typealias Lister = () -> Items
    public typealias Configurator = (inout FormListConfiguration) -> ()
    
    fileprivate var allItems: Items
    fileprivate var valuesSection: Int = 0
    fileprivate var nilSection: Int? = nil
    fileprivate var nilName: String? = nil
    fileprivate var selectedIndices: IndexSet = [] { didSet {
        if selectedIndices.isEmpty {
            (view as? FormTextFieldWithoutCaret)?.text = nilName
        }
        else {
            (view as? FormTextFieldWithoutCaret)?.text = selectedIndices.map({ conf.flattenInField ? flattenedName(at: $0) : allItems[$0].name }).joined(separator: conf.separator)
        }
    }}
    fileprivate var allowedSelectionCount: CountableClosedRange<Int>
    
    fileprivate var lister: Lister
    fileprivate var conf: FormListConfiguration
    fileprivate weak var navigationVC: UINavigationController?
    
    
    
    //MARK: - KeyPath<Model, Value?>
    public init(keyPath: WritableKeyPath<Model, Value?>, configurator: Configurator? = nil, lister: @escaping Lister, pushOn navigationVC: UINavigationController?) {
        var conf = FormListConfiguration(); configurator?(&conf)
        
        self.lister = lister
        self.conf = conf
        self.allItems = lister()
        self.navigationVC = navigationVC
        
        switch conf.nilMode {
            case .noSelection(let name):
                self.valuesSection = 0
                self.nilSection = nil
                self.nilName = name
                self.allowedSelectionCount = 0 ... 1
            
            case .first(let name):
                self.valuesSection = 1
                self.nilSection = 0
                self.nilName = name
                self.allowedSelectionCount = 1 ... 1
            
            case .last(let name):
                self.valuesSection = 0
                self.nilSection = 1
                self.nilName = name
                self.allowedSelectionCount = 1 ... 1
            
        }
        
        let tf = FormTextFieldWithoutCaret(frame: .zero)
        super.init(tf, [.wantsDisclosureIndicator])
        tf.delegate = self
        
        
        syncFromModelToInput = { model, input in
            let f = input as! FormListField
            let value = model[keyPath: keyPath]
            
            if let v = value, let index = f.allItems.firstIndex(where: { $0.value == v }) {
                f.selectedIndices = [index]
            }
            else {
                f.selectedIndices = []
            }
        }
        
        syncFromInputToModel = { input, model in
            let f = input as! FormListField
            model[keyPath: keyPath] = f.selectedIndices.first.map({ f.allItems[$0].value })
        }
    }
    
    //MARK: - KeyPath<Model, Value>
    public init(keyPath: WritableKeyPath<Model, Value>, configurator: Configurator? = nil, lister: @escaping Lister, pushOn navigationVC: UINavigationController?) {
        var conf = FormListConfiguration(); configurator?(&conf)
        
        self.lister = lister
        self.conf = conf
        self.navigationVC = navigationVC
        self.allItems = lister()
        self.valuesSection = 0
        self.nilSection = nil
        self.allowedSelectionCount = 1 ... 1
        
        let tf = FormTextFieldWithoutCaret(frame: .zero)
        super.init(tf, [.wantsDisclosureIndicator])
        tf.delegate = self
        
        
        syncFromModelToInput = { model, input in
            let f = input as! FormListField
            let value = model[keyPath: keyPath]
            
            if let index = f.allItems.firstIndex(where: { $0.value == value }) {
                f.selectedIndices = [index]
            }
            else {
                // should not happen
                print("Whoops. This should not happen, a value should always be set here.")
                f.selectedIndices = []
            }
        }
        
        syncFromInputToModel = { input, model in
            let f = input as! FormListField
            guard let first = f.selectedIndices.first else { return }
            model[keyPath: keyPath] = f.allItems[first].value
        }
    }
    
    //MARK: - KeyPath<Model, [Value]>
    
    public init(keyPath: WritableKeyPath<Model, [Value]>, configurator: Configurator? = nil, lister: @escaping Lister, pushOn navigationVC: UINavigationController?) {
        var conf = FormListConfiguration(); configurator?(&conf)
        
        self.lister = lister
        self.conf = conf
        self.navigationVC = navigationVC
        self.allItems = lister()
        self.valuesSection = 0
        self.nilSection = nil
        self.allowedSelectionCount = conf.minChoices ... conf.maxChoices
        
        let tf = FormTextFieldWithoutCaret(frame: .zero)
        super.init(tf, [.wantsDisclosureIndicator])
        tf.delegate = self
        
        
        syncFromModelToInput = { model, input in
            let f = input as! FormListField
            let values = model[keyPath: keyPath]
            let indexes = values.compactMap({ v in return f.allItems.firstIndex(where: { $0.value == v }) })
            f.selectedIndices = IndexSet(indexes)
        }
        
        syncFromInputToModel = { input, model in
            let f = input as! FormListField
            model[keyPath: keyPath] = f.selectedIndices.map({ f.allItems[$0].value })
        }
    }

    //MARK: - Cell and text field
    public override func didTapParentCell() {
        view.becomeFirstResponder()
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let row = controller?.rowWithControl(view) else { return false }
        if conf.reloadListEveryTime {
            allItems = lister()
            // TODO what if I had a value and it's no longer there? my selectedIndices will be fd up
            // better call syncFromModelToInput here
        }
        let vc = listViewController()
        if let t = row.title { vc.navigationItem.title = t }
        navigationVC?.pushViewController(vc, animated: true)
        controller?.rowDidBeginEditing(row)
        return false
    }
    
    //MARK: - Accessors for items
    internal func flattenedName(at index: Int) -> String {
        guard allItems.indices.contains(index) else { return "" }
        let item = allItems[index]
        guard index > 0, item.indentationLevel > 0 else { return item.name }
        
        var stack = [item.name]
        var depth = item.indentationLevel
        for other in allItems[0...index-1].reversed() {
            if other.indentationLevel < depth {
                depth = other.indentationLevel
                stack.insert(other.name, at: 0)
            }
            guard other.indentationLevel > 0 else { break }
        }
        
        return stack.joined(separator: conf.flattener)
    }
    
    //MARK: - Bridge with child table view controller
    internal func listViewController() -> FKDynamicListTVC<Value> {
        return FKDynamicListTVC(parent: self)
    }

    internal func listVCDidDismiss() {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
    
    //MARK: - Table view D/DS
    public func numberOfSections(in tableView: UITableView) -> Int {
        return (nilSection == nil) ? 1 : 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case nilSection: return 1
            default: return allItems.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? {
            let c = UITableViewCell(style: .default, reuseIdentifier: "cell")
            c.indentationWidth = 33
            return c
        }()
        
        switch indexPath.section {
            case nilSection:
                cell.textLabel?.text = nilName ?? "ø"
                cell.imageView?.image = nil
                cell.indentationLevel = 0
                cell.accessoryType = selectedIndices.isEmpty ? .checkmark : .none
            default:
                let item = allItems[indexPath.row]
                cell.textLabel?.text = item.name
                cell.imageView?.image = item.image
                cell.indentationLevel = item.indentationLevel
                cell.accessoryType = selectedIndices.contains(indexPath.row) ? .checkmark : .none
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var ipsToReload = [IndexPath]()
        var dismiss = false
        let selCount = selectedIndices.count
        
        // Easy exit first: tapping on the only nil row
        if indexPath.section == nilSection {
            ipsToReload.append(contentsOf: selectedIndices.map({ IndexPath(row: $0, section: valuesSection) }))
            selectedIndices = []
            ipsToReload.append(indexPath)
            dismiss = true
        }
        
        // Tapping on a previously selected row
        else if selectedIndices.contains(indexPath.row) {
            // Respect minimum
            guard allowedSelectionCount.contains(selCount - 1) else { return }
            selectedIndices.remove(indexPath.row)
            ipsToReload.append(indexPath)
            
            // Dismiss for single-selection lists
            dismiss = (allowedSelectionCount.upperBound == 1)
        }
        
        // Tapping on a previously unselected row, in single-selection list
        else if allowedSelectionCount.upperBound == 1 {
            if let nilSection = nilSection, selCount == 0 {
                ipsToReload.append(IndexPath(row: 0, section: nilSection))
            }
            else {
                ipsToReload.append(contentsOf: selectedIndices.map({ IndexPath(row: $0, section: valuesSection) }))
            }
            selectedIndices = [indexPath.row]
            ipsToReload.append(indexPath)
            dismiss = true
        }
        
        // Tapping on a previously unselected row, in multiple-selection list
        else {
            guard allowedSelectionCount.contains(selCount + 1) else { return }
        }
        
        // Then, perform!
        tableView.reloadRows(at: ipsToReload, with: .automatic)
        if dismiss {
            tableView.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.33) { [weak self] in
                self?.navigationVC?.popViewController(animated: true)
            }
        }
    }
}


extension FormListField where Value: CaseIterable & CustomStringConvertible {
    internal static func listerFor(_ type: Value.Type) -> () -> [FormListItem<Value>] {
        return {
            return Value.allCases.map({
                let image: UIImage? = ($0 as? DescribableWithIcon)?.icon(for: CGSize(width: 28, height: 28))
                return FormListItem(name: $0.description, image: image, value: $0, indentationLevel: 0, selectable: true)
            })
        }
    }
    
    public convenience init(keyPath: WritableKeyPath<Model, Value?>, configurator: Configurator? = nil, pushOn nav: UINavigationController?) {
        self.init(keyPath: keyPath, configurator: configurator, lister: FormListField.listerFor(Value.self), pushOn: nav)
    }
    public convenience init(keyPath: WritableKeyPath<Model, Value>, configurator: Configurator? = nil, pushOn nav: UINavigationController?) {
        self.init(keyPath: keyPath, configurator: configurator, lister: FormListField.listerFor(Value.self), pushOn: nav)
    }
    public convenience init(keyPath: WritableKeyPath<Model, [Value]>, configurator: Configurator? = nil, pushOn nav: UINavigationController?) {
        self.init(keyPath: keyPath, configurator: configurator, lister: FormListField.listerFor(Value.self), pushOn: nav)
    }
    
}


internal class FKDynamicListTVC<Value>: UITableViewController where Value: Equatable {
    var dismissCallback: () -> Void
    
    init<Model>(parent: FormListField<Model, Value>) {
        self.dismissCallback = { [weak parent] in
            parent?.listVCDidDismiss()
        }
        super.init(style: .insetGrouped)
        tableView.delegate = parent
        tableView.dataSource = parent
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent || isBeingDismissed {
            dismissCallback()
        }
    }
}
