import UIKit

public class FormSwitch<Model>: FormInput<Model> {
    
    public init(getter: @escaping () -> Bool, setter: @escaping (Bool) -> (), switchClass: FormSwitchable.Type = UISwitch.self) {
        
        let field = switchClass.init(frame: .zero)
        guard let f = field as? UIView else { preconditionFailure("\(String(describing: switchClass)) must be a UIView subclass.") }
        
        super.init(f, [.compact])
        
        if let ctl = f as? UIControl {
            ctl.addTarget(self, action: #selector(didTapSwitch(_:)), for: .valueChanged)
        }
        
        syncFromModelToInput = { model, input in
            guard let s = input.view as? FormSwitchable else { return }
            s.setOn(getter(), animated: false)
        }
        
        syncFromInputToModel = { input, model in
            guard let s = input.view as? FormSwitchable else { return }
            setter(s.isOn)
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, Bool>, switchClass: FormSwitchable.Type = UISwitch.self) {
        
        let field = switchClass.init(frame: .zero)
        guard let f = field as? UIView else { preconditionFailure("\(String(describing: switchClass)) must be a UIView subclass.") }
        
        super.init(f, [.compact])
        
        if let ctl = f as? UIControl {
            ctl.addTarget(self, action: #selector(didTapSwitch(_:)), for: .valueChanged)
        }
    
        syncFromModelToInput = { model, input in
            guard let s = input.view as? FormSwitchable else { return }
            s.setOn(model[keyPath: keyPath], animated: false)
        }
        
        syncFromInputToModel = { input, model in
            guard let s = input.view as? FormSwitchable else { return }
            model[keyPath: keyPath] = s.isOn
        }
    }
    
    @objc private func didTapSwitch(_ sender: Any) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
    
    public override func didTapParentCell() {
        guard let s = view as? FormSwitchable else { return }
        s.setOn(!s.isOn, animated: true)
        didTapSwitch(s)
    }
    
}



public protocol FormSwitchable: class {
    var isOn: Bool { get }
    func setOn(_ on: Bool, animated: Bool) -> Void
    init(frame: CGRect)
    // must also send a .valueChanged target-action, just like UISwitch
}

extension UISwitch: FormSwitchable {}


open class FormCheckmark: UIControl, FormSwitchable {
    private static let size = CGSize(width: 22, height: 22)
    
    public required override init(frame: CGRect) {
        self.isOn = false
        super.init(frame: CGRect(origin: frame.origin, size: FormCheckmark.size))
        tintColorDidChange()
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        addGestureRecognizer(tgr)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override var intrinsicContentSize: CGSize { return FormCheckmark.size }
    
    public var isOn: Bool { didSet {
        setNeedsLayout()
    }}
    
    public func setOn(_ on: Bool, animated: Bool) {
        isOn = on
    }
    
    open override func tintColorDidChange() {
        super.tintColorDidChange()
        
        offCircle.strokeColor = tintColor.cgColor
        onCircle.fillColor = tintColor.cgColor
    }
    
    @objc func didTap(_ rec: UIGestureRecognizer) {
        setOn(!isOn, animated: true)
        sendActions(for: .valueChanged)
    }
    
    // MARK: - Layers
    private lazy var offCircle: CAShapeLayer = {
        let ret = CAShapeLayer()
        ret.strokeColor = UIColor.black.cgColor
        ret.fillColor = UIColor.clear.cgColor
        ret.lineWidth = 1
        ret.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 22, height: 22).insetBy(dx: 0.5, dy: 0.5)).cgPath
        return ret
    }()
    
    private var onCircle: CAShapeLayer = {
        let ret = CAShapeLayer()
        ret.fillColor = UIColor.black.cgColor
        ret.strokeColor = UIColor.clear.cgColor
        ret.lineWidth = 0.0
        
        var path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 22, height: 22))
        
        let t = CGAffineTransform.identity
            .translatedBy(x: 11, y: 11)
            .rotated(by: CGFloat(-Double.pi / 4))
            .translatedBy(x: -11, y: -11)
        
        let v = UIBezierPath(rect: CGRect(x: 6, y: 7, width: 2, height: 4))
        v.append(UIBezierPath(rect: CGRect(x: 6, y: 11, width: 12, height: 2)))
        v.apply(t)
        path.append(v)
        
        ret.path = path.cgPath
        ret.fillRule = .evenOdd
        return ret
    }()
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if offCircle.superlayer == nil {
            layer.addSublayer(offCircle)
        }
        if onCircle.superlayer == nil {
            layer.addSublayer(onCircle)
        }
        
        let c = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        let r = CGRect(x: 0, y: 0, width: 22, height: 22)
        
        offCircle.bounds    = r
        offCircle.position  = c
        offCircle.transform = isOn ? CATransform3DMakeScale(0.1, 0.1, 1) : CATransform3DIdentity
        offCircle.opacity   = isOn ? 0 : 1
        
        onCircle.bounds     = r
        onCircle.position   = c
        onCircle.transform  = isOn ? CATransform3DIdentity : CATransform3DConcat(CATransform3DMakeScale(0.1, 0.1, 1), CATransform3DMakeRotation(CGFloat(-Double.pi / 2), 0, 0, 1))
        onCircle.opacity    = isOn ? 1 : 0
    }
    
}
