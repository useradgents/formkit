import UIKit

// This one can be subclassed if you've special needs for special fields

public struct FormInputOptions: OptionSet {
    
    public typealias RawValue = Int
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let interactive = FormInputOptions(rawValue: 1 << 0)
    public static let compact = FormInputOptions(rawValue: 1 << 1)
    public static let wantsDisclosureIndicator = FormInputOptions(rawValue: 1 << 2)
    public static let readOnly = FormInputOptions(rawValue: 1 << 3)
}


open class FormInput<Model>: NSObject {
    
    public internal(set) var view: UIView
    public var options: FormInputOptions

    public init(_ view: UIView, _ options: FormInputOptions = []) {
        self.view = view
        self.options = options
        
        if options.contains(.compact) {
            view.setContentHuggingPriority(.required, for: .horizontal)
        }

        view.isUserInteractionEnabled = options.isDisjoint(with: [.readOnly])
    }
    
    public weak var controller: FormController<Model>?
    
    open func didTapParentCell() {}
    
    // Accessors
    //
    public var syncFromModelToInput: ((Model, FormInput<Model>) -> Void)?
    public var syncFromInputToModel: ((FormInput<Model>, inout Model) -> Void)?
}

public class FormLabel<Model>: FormInput<Model> {
    public init(text: String? = nil, configurator: ((UILabel) -> ())? = nil) {
        let l = UILabel(frame: .zero)
        super.init(l, [.readOnly])
        
        setup()
        l.text = text
        configurator?(l)
        
        syncFromModelToInput = { _, _ in }
        syncFromInputToModel = { _, _ in }
    }
    
    internal func setup() {
        guard let l = view as? UILabel else { return }
        l.numberOfLines = 1
        l.textColor = .secondaryLabel
        l.textAlignment = .right
    }
}

// TODO

public class FormStepper<Model>: FormInput<Model> {}

public class FormImageField<Model>: FormInput<Model> {}

public class FormColorField<Model>: FormInput<Model> {}
