import UIKit

public class FormPasswordField<Model>: FormInput<Model>, UITextFieldDelegate {
    
    private var emptyMeansNil: Bool = true
    private var emptyHash: String
    private var hasher: (String)->String
    private var modified: Bool = false
    
    public init(keyPath: WritableKeyPath<Model, String>, hasher: @escaping (String)->String, configurator: ((UITextField)->())? = nil) {
        self.hasher = hasher
        self.emptyHash = hasher("")
        
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let i = input as? FormPasswordField<Model> else { return }
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath] == i.emptyHash ? "" : "Scrambled"
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormPasswordField<Model> else { return }
            guard i.modified else { return }
            guard let tf = input.view as? UITextField else { return }
            model[keyPath: keyPath] = i.hasher(tf.text ?? "")
        }
    }
    
    public init(keyPath: WritableKeyPath<Model, String?>, hasher: @escaping (String)->String, emptyMeansNil: Bool = true, configurator: ((UITextField)->())? = nil) {
        self.emptyMeansNil = emptyMeansNil
        self.hasher = hasher
        self.emptyHash = hasher("")
        
        let f = UITextField(frame: .zero)
        super.init(f, [.interactive])
        
        setup()
        configurator?(f)
        
        syncFromModelToInput = { model, input in
            guard let i = input as? FormPasswordField<Model> else { return }
            guard let tf = input.view as? UITextField else { return }
            tf.text = model[keyPath: keyPath] == nil || model[keyPath: keyPath] == i.emptyHash ? "" : "Scrambled"
        }
        
        syncFromInputToModel = { input, model in
            guard let i = input as? FormPasswordField<Model> else { return }
            guard i.modified else { return }
            guard let tf = i.view as? UITextField else { return }
            
            if let text = tf.text, !text.isEmpty {
                model[keyPath: keyPath] = i.hasher(text)
            }
            else {
                model[keyPath: keyPath] = i.emptyMeansNil ? nil : i.hasher("")
            }
            
        }
    }
    
    internal func setup() {
        guard let tf = view as? UITextField else { return }
        tf.clearButtonMode = .whileEditing
        tf.clearsOnBeginEditing = true
        tf.clearsOnInsertion = true
        tf.isSecureTextEntry = true
        tf.delegate = self
        tf.addTarget(self, action: #selector(textFieldDidChangeEditing(_:)), for: .editingChanged)
    }
    
    public override func didTapParentCell() {
        view.becomeFirstResponder()
    }
    
    //MARK: - Text field delegate
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let row = controller?.rowWithControl(view) else { return false }
        controller?.rowDidBeginEditing(row)
        return true
    }
    
    @objc private func textFieldDidChangeEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        modified = true
        controller?.rowDidEdit(row, isStillEditing: true)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let row = controller?.rowWithControl(view) else { return }
        controller?.rowDidEdit(row, isStillEditing: false)
    }
    
}
