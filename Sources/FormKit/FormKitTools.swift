import UIKit

extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 0, height: 0)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage, scale: image!.scale, orientation: image!.imageOrientation)
    }
}


class Weak<T> where T: AnyObject {
    private(set) weak var object: T?
    
    init(_ object: T?) {
        self.object = object
    }
}

extension Array where Element: AnyObject {
    func weakify() -> Array<Weak<Element>> {
        return map({ Weak($0) })
    }
}


public class FormTextFieldWithoutCaret: UITextField {
    open override func caretRect(for _: UITextPosition) -> CGRect {
        return .zero
    }
}



extension FormController {
    internal func chevronDownImage() -> UIImage {
        let size = CGSize(width: 21, height: 12)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        UIColor.black.setFill()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 1.25))
        path.addLine(to: CGPoint(x: 1.25, y: 0))
        path.addLine(to: CGPoint(x: 10.5, y: 9.25))
        path.addLine(to: CGPoint(x: 19.75, y: 0))
        path.addLine(to: CGPoint(x: 21, y: 1.25))
        path.addLine(to: CGPoint(x: 10.5, y: 11.75))
        path.addLine(to: CGPoint(x: 0, y: 1.25))
        path.close()
        path.fill()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { fatalError() }
        return UIImage(cgImage: cgImage, scale: image!.scale, orientation: image!.imageOrientation).withRenderingMode(.alwaysTemplate)

    }
    
    internal func chevronUpImage() -> UIImage {
        let size = CGSize(width: 21, height: 12)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        UIColor.black.setFill()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 12-1.25))
        path.addLine(to: CGPoint(x: 1.25, y: 12-0))
        path.addLine(to: CGPoint(x: 10.5, y: 12-9.25))
        path.addLine(to: CGPoint(x: 19.75, y: 12-0))
        path.addLine(to: CGPoint(x: 21, y: 12-1.25))
        path.addLine(to: CGPoint(x: 10.5, y: 12-11.75))
        path.addLine(to: CGPoint(x: 0, y: 12-1.25))
        path.close()
        path.fill()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { fatalError() }
        return UIImage(cgImage: cgImage, scale: image!.scale, orientation: image!.imageOrientation).withRenderingMode(.alwaysTemplate)
    }
    
    internal func accessoryAddButtonImage() -> UIImage {
        let size = CGSize(width: 14, height: 14)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        UIColor.systemBlue.setFill()
        let path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: size), cornerRadius: 3)
        
        path.usesEvenOddFillRule = true
        path.move(to: CGPoint(x: 6, y: 3))
        path.addLine(to: CGPoint(x: 8, y: 3))
        path.addLine(to: CGPoint(x: 8, y: 6))
        path.addLine(to: CGPoint(x: 11, y: 6))
        path.addLine(to: CGPoint(x: 11, y: 8))
        path.addLine(to: CGPoint(x: 8, y: 8))
        path.addLine(to: CGPoint(x: 8, y: 11))
        path.addLine(to: CGPoint(x: 6, y: 11))
        path.addLine(to: CGPoint(x: 6, y: 8))
        path.addLine(to: CGPoint(x: 3, y: 8))
        path.addLine(to: CGPoint(x: 3, y: 6))
        path.addLine(to: CGPoint(x: 6, y: 6))
        path.close()
        path.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { fatalError() }
        return UIImage(cgImage: cgImage, scale: image!.scale, orientation: image!.imageOrientation) //.withRenderingMode(.alwaysTemplate)
    }
}


public protocol DescribableWithIcon {
    func icon(for size: CGSize) -> UIImage?
}

