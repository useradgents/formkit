import UIKit

public class FormRow<Model> {

    internal var tag: Int

    init(tag: Int) {
        self.tag = tag
    }

    public var title: String?
    public var image: UIImage?
    public var input: FormInput<Model>?
    
    internal var indexPath: IndexPath?
}

extension FormController {
    public func rowWithControl(_ input: UIView) -> FormRow<Model>? {
        for s in sections {
            for r in s.rows {
                if r.input?.view == input {
                    return r
                }
            }
        }
        return nil
    }
    
    internal func rowAtIndexPath(_ indexPath: IndexPath) -> FormRow<Model>? {
        for s in sections {
            for r in s.rows {
                if r.indexPath == indexPath {
                    return r
                }
            }
        }
        return nil
    }
}
