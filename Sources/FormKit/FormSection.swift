public class FormSection<Model> {

    internal var tag: Int
    internal var rows: [FormRow<Model>]
    
    internal var isArray: Bool = false
    internal var canAddRows: Bool = false
    internal var canDeleteRows: Bool = false
    internal var canReorderRows: Bool = false

    init(tag: Int) {
        self.tag = tag
        rows = []
    }
    
    func prepareRows(from model: Model) {
        rows.forEach { (row) in
            guard let input = row.input else { return }
            input.syncFromModelToInput?(model, input)
        }
    }
    
    func addNewRowToArray(of model: inout Model) -> FormRow<Model> {
        fatalError()
    }
    
    func deleteRowForArray(of model: inout Model, at index: Int) -> FormRow<Model> {
        fatalError()
    }
    
    func moveRowForArray(of model: inout Model, from sourceIndex: Int, to newIndex: Int, inSectionAtIndex section: Int) -> FormRow<Model> {
        fatalError()
    }

    public var title: String?
    public var footerText: String?
    public var addRowTitle: String?
}

public class FormArraySection<Model, Element>: FormSection<Model> {
    public var baseRowTag: Int = 1000
    
    public var minRows: Int = 0
    public var maxRows: Int = Int.max
    
    public let arrayKeyPath: WritableKeyPath<Model, [Element]>
    public var inputMaker: ((Int) -> FormInput<Model>)?
    public var newInstanceMaker: (() -> Element)?
    
    init(tag: Int, _ arrayKeyPath: WritableKeyPath<Model, [Element]>) {
        self.arrayKeyPath = arrayKeyPath
        super.init(tag: tag)
        super.isArray = true
        super.canAddRows = true
        super.canDeleteRows = true
        super.canReorderRows = true
    }
    
    override func prepareRows(from model: Model) {
        let countFromModel = model[keyPath: arrayKeyPath].count
        if rows.count < countFromModel {
            rows.append(contentsOf: (rows.count ..< countFromModel).map({ i in
                let row = FormRow<Model>(tag: baseRowTag + i)
                row.title = "#\(i + 1)"
                row.input = inputMaker?(i)
                return row
            }))
        }
        else if rows.count > countFromModel {
            rows.removeLast(rows.count - countFromModel)
        }
        
        super.prepareRows(from: model)
    }
    
    override func addNewRowToArray(of model: inout Model) -> FormRow<Model> {
        guard let m = newInstanceMaker else { fatalError() }
        let index = model[keyPath: arrayKeyPath].count
        
        model[keyPath: arrayKeyPath].append(m())
        
        let row = FormRow<Model>(tag: baseRowTag + index)
        row.title = "#\(index + 1)"
        row.input = inputMaker?(index)
        return row
    }
    
    override func deleteRowForArray(of model: inout Model, at index: Int) -> FormRow<Model> {
        let row = rows[index]
        model[keyPath: arrayKeyPath].remove(at: index)
        rows.remove(at: index)
        return row
    }
    
    override func moveRowForArray(of model: inout Model, from sourceIndex: Int, to newIndex: Int, inSectionAtIndex section: Int) -> FormRow<Model> {
        let foo = model[keyPath: arrayKeyPath][sourceIndex]
        model[keyPath: arrayKeyPath].remove(at: sourceIndex)
        model[keyPath: arrayKeyPath].insert(foo, at: newIndex)
        
        return rows[newIndex]
    }
}
