import UIKit

public class FormController<Model>: NSObject, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {

    // MARK: - Initializer

    public init(tableView: UITableView) {
        self.tableView = tableView
        super.init()

        setupTableView()
        setupLongPressRecognizer()
    }
    
    deinit {
        //print("So long, and thanks for all the foobar.")
    }

    // MARK: - Public interface

    public internal(set) weak var tableView: UITableView?
    public weak var delegate: FormControllerDelegate?
    
    public var clearsErrorsWhenEditingBegins: Bool = true
    
    public var editArraysButton: UIBarButtonItem?

    // MARK: - Private storage

    internal var sections: [FormSection<Model>] = []
    
    // MARK: - Editing an instance of a Model
    public internal(set) var originalInstance: Model?
    public /*internal(set)*/ var editedInstance: Model? // relaxed set for out-of-module custom fields
    
    public func edit(_ instance: Model) {
        guard originalInstance == nil, editedInstance == nil else { return }
        originalInstance = instance
        
        switch Mirror(reflecting: instance).displayStyle {
        case .some(.class):
            if let copiableInstance = instance as? NSCopying, let copy = copiableInstance.copy(with: nil) as? Model {
                editedInstance = copy
            }
            else {
                editedInstance = instance
                print("💣 Editing an uncopiable object -- this is guaranteed to have inconsistent behavior.")
                print("⚠️ \(String(reflecting: type(of: instance))) needs to conform to NSCopying.")
            }
        default:
            editedInstance = instance
        }
        
        if let _ = sections.first(where: { $0.canAddRows || $0.canReorderRows || $0.canDeleteRows }) {
            editArraysButton = UIBarButtonItem(title: "Edit arrays", style: .plain, target: self, action: #selector(didTapEditArrayButton(_:)))
        }
        
        syncEverythingFromModelToInputs()
//        updateNavigation()
    }
    
    func syncEverythingFromModelToInputs() {
        guard let instance = editedInstance else { return }
        
        for s in sections {
            s.prepareRows(from: instance)
        }
        
        tableView?.reloadData()
        bindAllInputs()
        updateFocusableFields()
    }
    
    public func startEditing() {
        for s in sections {
            for r in s.rows {
                guard let i = r.input else { continue }
                if i.options.contains(.interactive) && i.options.isDisjoint(with: [.readOnly]) {
                    i.view.becomeFirstResponder()
                }
                return
            }
        }
    }
    
    public var isEditingArrays: Bool {
        set { tableView?.setEditing(newValue, animated: true) }
        get { return tableView?.isEditing ?? false }
    }

    @objc func didTapEditArrayButton(_ button: UIBarButtonItem) {
        isEditingArrays = !isEditingArrays
        switch isEditingArrays {
            case true: button.title = "End editing"
            case false: button.title = "Edit arrays"
        }
    }
    
    // MARK: - Handling errors
    public func removeAllErrors() {
        
    }
    
    // MARK: - Toolbar above interactive fields
    
    internal var focusableFields: [Weak<UIView>] = []
    
    internal weak var focusedInput: UIView? { didSet { updateToolbarPrevNext() }}
    
    internal func updateFocusableFields() {
        focusableFields = sections.flatMap{ $0.rows.compactMap {
            guard let i = $0.input, i.options.contains(.interactive), !i.options.contains(.readOnly) else { return nil }
            return i.view
        }}.weakify()
    }
    
    internal func updateToolbarPrevNext() {
        guard let input = focusedInput else { return }
        guard let idx = focusableFields.firstIndex(where: { $0.object == input }) else { return }
        
        let count = focusableFields.count
        toolbarPrev.isEnabled = count >= 2 && idx > 0
        toolbarNext.isEnabled = count >= 2 && idx < count-1
    }
    
    
    internal func setupToolbar(for textField: UITextField?) {
        guard let textField = textField else { return }
        if UIDevice.current.userInterfaceIdiom == .pad {
            textField.inputAssistantItem.allowsHidingShortcuts = false
            textField.inputAssistantItem.leadingBarButtonGroups = [
                UIBarButtonItemGroup(barButtonItems: [
                    UIBarButtonItem(image: chevronUpImage(),  style: .plain, target: self, action: #selector(didTapToolbarPrev(_:))),
                    UIBarButtonItem(image: chevronDownImage(),  style: .plain, target: self, action: #selector(didTapToolbarNext(_:)))
                ], representativeItem: nil)
            ]
            textField.inputAssistantItem.trailingBarButtonGroups = [
                UIBarButtonItemGroup(barButtonItems: [
                    UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didTapToolbarDone(_:)))
                ], representativeItem: nil)
            ]
        }
        else {
            textField.inputAccessoryView = toolbar
        }
    }
    
    internal lazy var toolbarPrev = UIBarButtonItem(image: chevronUpImage(),  style: .plain, target: self, action: #selector(didTapToolbarPrev(_:)))
    internal lazy var toolbarNext = UIBarButtonItem(image: chevronDownImage(),  style: .plain, target: self, action: #selector(didTapToolbarPrev(_:)))
    
    internal lazy var toolbar: UIToolbar = {
        let gap = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didTapToolbarDone(_:)))
        
        let tb = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        tb.setItems([toolbarPrev, toolbarNext, gap, done], animated: false)
        return tb
    }()
    
    @objc func didTapToolbarPrev(_ sender: UIBarButtonItem) {
        guard let input = focusedInput else { return }
        guard let idx = focusableFields.firstIndex(where: { $0.object == input }) else { return }
        guard idx > 0 else { return }
        
        focusableFields[idx - 1].object?.becomeFirstResponder()
    }
    
    @objc func didTapToolbarNext(_ sender: UIBarButtonItem) {
        guard let input = focusedInput else { return }
        guard let idx = focusableFields.firstIndex(where: { $0.object == input }) else { return }
        guard idx < focusableFields.count - 1 else { return }
        
        focusableFields[idx + 1].object?.becomeFirstResponder()
    }
    
    @objc func didTapToolbarDone(_ sender: UIBarButtonItem) {
        focusedInput?.resignFirstResponder()
    }
    
    



    // MARK: - Table view Delegate + DataSource forwarding
    // We'll just forward those to our internal extension.
    // We cannot (yet) conform to UITVD/DS in an extension, because "@objc is not
    // supported within extensions of generic classes or classes that inherit from generic classes". F*ck you, UIKit.

    public func numberOfSections(in tableView: UITableView) -> Int {
        return fkNumberOfSections(in: tableView)
    }

    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fkTableView(tableView, titleForHeaderInSection: section)
    }

    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return fkTableView(tableView, titleForFooterInSection: section)
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fkTableView(tableView, numberOfRowsInSection: section)
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return fkTableView(tableView, cellForRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return fkTableView(tableView, canEditRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        fkTableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return fkTableView(tableView, canMoveRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return fkTableView(tableView, targetIndexPathForMoveFromRowAt: sourceIndexPath, toProposedIndexPath: proposedDestinationIndexPath)
    }
    
    public func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        fkTableView(tableView, moveRowAt: sourceIndexPath, to: destinationIndexPath)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fkTableView(tableView, didSelectRowAt: indexPath)
    }
    

    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard let row = rowWithControl(textField) else { return false }

        textField.text = ""
        rowDidEdit(row, isStillEditing: true)
        return true
    }
    
    
    // MARK: - Editing fields
    public func rowDidBeginEditing(_ row: FormRow<Model>) {
        if let ip = row.indexPath {
            tableView?.scrollToRow(at: ip, at: .none, animated: true)
        }
        
        if clearsErrorsWhenEditingBegins {
            removeAllErrors()
        }
        
        if let input = row.input, (input.view is UITextField || input.view is UITextView) {
            focusedInput = input.view
        }
        else {
            focusedInput = nil
        }
        
        delegate?.formController(willEditFieldInRowWithTag: row.tag)
    }
    
    
    public func endEditing() {
        focusedInput?.resignFirstResponder()
    }
    

    // MARK: - Long press recognizing
    
    private func setupLongPressRecognizer() {
        let lpr = UILongPressGestureRecognizer(target: self, action: #selector(longPressRecognizerHandler(_:)))
        lpr.delegate = self
        tableView?.addGestureRecognizer(lpr)
    }
    
    @objc func longPressRecognizerHandler(_ sender: UILongPressGestureRecognizer) {
        guard sender.state == .began else { return }
        guard let tv = tableView else { return }
        tv.setEditing(!tv.isEditing, animated: true)
        sender.state = .cancelled
    }
    
    // Only allow long press on all rows of an array sections, except the Add row.
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let tv = tableView else { return false }
        
        // end editing: anywhere
        if tv.isEditing { return true }
        
        // start editing: only on movable/deletable cells
        guard let ip = tv.indexPathForRow(at: touch.location(in: tv)) else { return false }
        guard sections[ip.section].canDeleteRows || sections[ip.section].canReorderRows else { return false }
        guard sections[ip.section].rows.indices.contains(ip.row) else { return false }

        return true
    }
    
    
    // MARK: - Generic field handling
    
    public func rowDidEdit(_ row: FormRow<Model>, isStillEditing: Bool, syncInput: Bool = true) {
        guard let input = row.input else { return }
        guard editedInstance != nil else { return }
        if syncInput {
            input.syncFromInputToModel?(input, &editedInstance!)
        }
        delegate?.formController(didEditFieldInRowWithTag: row.tag, isStillEditing: isStillEditing)
    }
    
    
}
