public protocol FormControllerDelegate: class {
    func formController(willEditFieldInRowWithTag tag: Int)
    func formController(didEditFieldInRowWithTag tag: Int, isStillEditing: Bool)
}

// Default implementation
public extension FormControllerDelegate {
    func formController(willEditFieldInRowWithTag tag: Int) {}
    func formController(didEditFieldInRowWithTag tag: Int, isStillEditing: Bool) {}
}
