import UIKit

public extension FormController {
    func bindAllInputs() {
        // Bind inputs of all rows of all sections
        sections.forEach { $0.rows.forEach {
            $0.input?.controller = self
            setupToolbar(for: $0.input?.view as? UITextField)
        } }
    }

    func addSection(_ tag: Int, _ configure: (FormSection<Model>) -> Void) {
        let section = FormSection<Model>(tag: tag)
        configure(section)
        sections.append(section)
    }
    
    func addArraySection<Element>(_ tag: Int, _ arrayKeyPath: WritableKeyPath<Model, [Element]>, _ configure: (FormArraySection<Model, Element>) -> Void) {
        let section = FormArraySection<Model, Element>(tag: tag, arrayKeyPath)
        configure(section)
        sections.append(section)
    }
}

public extension FormSection {
    func addRow(_ tag: Int, _ configure: (FormRow<Model>) -> Void) {
        let row = FormRow<Model>(tag: tag)
        configure(row)
        rows.append(row)
    }
    
    func addRow(_ tag: Int, _ title: String, _ input: FormInput<Model>) {
        let row = FormRow<Model>(tag: tag)
        row.title = title
        row.input = input
        rows.append(row)
    }
}
