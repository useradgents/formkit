import UIKit

open class FormSegmentedControl: UISegmentedControl {
    
    // Publicly added functionality
    @IBInspectable public var minimumSelectedSegmentsCount: Int = 0
    @IBInspectable public var maximumSelectedSegmentsCount: Int = Int.max
    
    open var selectedSegmentIndexes: IndexSet {
        get {
            guard let segs = pwndSegments else { return [] }
            return IndexSet(segs.enumerated().filter({ $1.value(forKey: "selected") as? Bool ?? false }).map({ $0.offset }))
        }
        set {
            guard let segs = pwndSegments else { return }
            segs.enumerated().forEach {
                $1.setValue(newValue.contains($0), forKey: "selected")
            }
            updateDividers()
        }
    }
    
    open func isSelectedForSegment(at segment: Int) -> Bool {
        guard let segs = pwndSegments else { return false }
        guard segs.indices.contains(segment) else { return false }
        return (segs[segment].value(forKey: "selected") as? Bool) ?? false
    }
    
    // Overrides to public properties and methods
    open override var isMomentary: Bool {
        get { return false }
        set { super.isMomentary = false }
    }
    
    open override func insertSegment(with image: UIImage?, at segment: Int, animated: Bool) {
        super.insertSegment(with: image, at: segment, animated: animated)
        updateDividers()
    }
    
    open override func insertSegment(withTitle title: String?, at segment: Int, animated: Bool) {
        super.insertSegment(withTitle: title, at: segment, animated: animated)
        updateDividers()
    }
    
    open override func removeSegment(at segment: Int, animated: Bool) {
        super.removeSegment(at: segment, animated: animated)
        updateDividers()
    }
    
    open override var selectedSegmentIndex: Int {
        get { return selectedSegmentIndexes.first ?? UISegmentedControl.noSegment }
        set { selectedSegmentIndexes = [newValue] }
    }
    
    // Inner workings
    private var pwndSegments: [UIView]? {
        return self.value(forKey: "segments") as? [UIView]
    }
    
    private func updateDividers() {
        guard let segs = pwndSegments else { return }
        (0..<segs.count-1).forEach {
            let selected = (segs[$0].value(forKey: "selected") as? Bool) ?? false
            let nextSelected = (segs[$0 + 1].value(forKey: "selected") as? Bool) ?? false
            segs[$0].setValue(!(selected && nextSelected), forKey: "showDivider")
        }
    }
    
    // Let the magic happen
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first, let segs = pwndSegments else {
            super.touchesEnded(touches, with: event)
            return
        }
        
        let point = touch.location(in: self)
        guard let segment = segs.enumerated().first(where: { (idx, segment) -> Bool in
            return segment.frame.contains(point)
        }) else {
            super.touchesEnded(touches, with: event)
            return
        }
        
        // OK, from now on we'll handle this ourselves, so let's cancel the touch
        super.touchesCancelled(touches, with: event)
        
        let selected = isSelectedForSegment(at: segment.offset)
        
        var newSelection = selectedSegmentIndexes
        if selected { newSelection.remove(segment.offset) }
        else { newSelection.insert(segment.offset) }
        
        if newSelection.count < minimumSelectedSegmentsCount {
            return
        }
        
        if newSelection.count > maximumSelectedSegmentsCount {
            if maximumSelectedSegmentsCount == 1 {
                // Behave like a traditional UISegmentedControl
                newSelection = [segment.offset]
            }
            else {
                return
            }
        }
        
        selectedSegmentIndexes = newSelection
        updateDividers()
        
        sendActions(for: .valueChanged)
    }
}
