public enum ListableNilMode {
    case noSelection(String?) // displayed on FKListFields only
    case first(String) // dislayed on FKListFields and FKSegments
    case last(String) // displayed on FKListFields and FKSegments
}

public protocol Listable: Equatable {
    static var allNames: [String] { get }
    static var allValues: [Self] { get }
    static var nilMode: ListableNilMode { get }
}

public protocol MultiSelectionListable: Listable {
    static var minChoices: Int { get }
    static var maxChoices: Int { get }
}

public extension Listable {
    static var nilMode: ListableNilMode { return ListableNilMode.noSelection(nil) }
}

public extension MultiSelectionListable {
    static var minChoices: Int { return 1 }
    static var maxChoices: Int { return Int.max }
}


